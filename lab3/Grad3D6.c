/*	Grad3D.c
 *	Examples:
 *      [fx, fy, ft] = Grad3D(bw_im1, bw_im);		
 *      [fx, fy]     = Grad3D(bw_im1);
 *where the images are assumed to images adjacent in a video stream.
 *		
 *  Notes:
 *      hard-coded for UINT8 images 
 *      grayscale only supported.
 *
 *  To Build:
 *      mex Grad3DX.c
 *
 *	Author:
 *		Stefan Karlsson
 *		stefan.karlsson@hh.se
 *      09/09/2011
 *
 *  Acknowledgement: Anthony Gabrielsons code from mathwork exchange: 
 *  'sobelgen' was the starting point for generic matlab mex interface
 */

#include "mex.h"
#include <string.h>             // Needed for memcpy() 

#define NDIMS           2       //X * Y

#define	SUCCESS			0
#define	MALLOCFAIL		-1
#define IMPROPERDIMS	-2

#define INWIDTH         128
#define FW              3       //filterwidth, deriv
#define HFW             1       //half filterwidth, deriv
#define OUTWIDTH        126     //OUTWIDTH = INWIDTH - HFW*2
#define INWIDTHC        63      //(INWIDTH/2)-HFW
#define OUTWIDTHC       61     //OUTWIDTH = INWIDTH - HFW*2

typedef unsigned char   uint8;
typedef __int32  			int32;
typedef __int16  			int16;

typedef struct image{
	uint8	*im;
	int     dims[NDIMS];
}image;

typedef struct image32{
	int32	*im;
	int     dims[NDIMS];    
}image32;

typedef struct progData{
        image bw_im1;
        image bw_im2; 
        image32 bw_im1C; 
        image32 bw_im2C; 
        image32 dx_im; 
        image32 dy_im; 
        image32 dt_im; 
        image32 dx_imC; 
        image32 dy_imC; 
        image32 dt_imC;
}progData;

int	gradCalc(progData *pD);  
int getData(   const mxArray **prhs, progData *pD, int nrhs);
int sendData( mxArray *plhs[], progData *pD);
int allocateMem(progData *pD);
int deAllocateMem(progData *pD);

/*
 *  mexFunction:  Matlab entry function into this C code
 *  Inputs: 
 *      int nlhs:   Number of left hand arguments (output)
 *      mxArray *plhs[]:   The left hand arguments (output)
 *      int nrhs:   Number of right hand arguments (inputs)
 *      const mxArray *prhs[]:   The right hand arguments (inputs)
 *
 * Notes:
 *      (Left)  goes_out = foo(goes_in);    (Right)
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	static int hasAllocated = 0;
	static	progData pD;
	int		status;
	//requires ONE or TWO input variables, the images i-1, i. 
	//If Called with zero variables, will cleanup memory.
    
   if( nrhs == 0 ){
       if(hasAllocated){
        	deAllocateMem(&pD);
			hasAllocated = 0;
        }
		return;
    }
    if (!hasAllocated){//then allocate
        if (allocateMem(&pD) == MALLOCFAIL){
            mexWarnMsgTxt("grad im malloc failed 2...\n");
            return;
        }
        hasAllocated = 1;
    }
    
    if( (status = getData(&prhs[0], &pD, nrhs)) != SUCCESS ){
         return;
    }
    if( gradCalc( &pD) != SUCCESS ){
        mexWarnMsgTxt("error gradCalc\n");
        return;
     }
    sendData( plhs, &pD);
//     printf("a\n");fflush(NULL);
   
    return;
}

int	gradCalc(progData *pD)
{
    int X, Y, H, W, HC, WC, ii, jj;
    int	sumX, sumY, sumT1,sumT2;
    uint8 *im1, *im2;
    int32 *dx, *dy, *dt, *dxC, *dyC, *dtC, *im1C, *im2C;
// #define FW  5
// #define HFW  2
// int at_mask[]=  { 3,  15,  27,  15,  3, 
//                  15,  82, 145,  82, 15,
//                  27, 145, 255, 145, 27,
//                  15,  82, 145,  82, 15,
//                   3,  15,  27,  15,  3};
// int d_mask[]=    {-10,  -53,  -93,  -53, -10, 
//                   -27, -145, -255, -145, -27,
//                     0,    0,    0,    0,   0,
//                    27,  145,  255,  145,  27, 
//                    10,   53,   93,   53,  10};
    
     int d_mask[]= {-3, -10, -3,
                     0,   0,  0,
                     3,  10,  3};
     int at_mask[]= {2,  4, 2,
                     4, 10, 4,
                     2,  4, 2};

     im1  = pD->bw_im1.im;
     im2  = pD->bw_im2.im;
     dx   = pD->dx_im.im;
     dy   = pD->dy_im.im;
     dt   = pD->dt_im.im;
     dxC  = pD->dx_imC.im;
     dyC  = pD->dy_imC.im;
     dtC  = pD->dt_imC.im;
     im1C = pD->bw_im1C.im;
     im2C = pD->bw_im2C.im;
    
    W = pD->bw_im1.dims[0];
    H = pD->bw_im1.dims[1];
    WC = pD->bw_im1C.dims[0];
    HC = pD->bw_im1C.dims[1];

    // 1st Convolution starts here
    for(Y=HFW; Y<H-HFW; Y++)  {
        for(X=HFW; X<W-HFW; X++)  {
			sumX = 0;  sumY = 0;  sumT1 = 0;sumT2 = 0;
			for (ii = -HFW; ii <= HFW;ii++){
				for (jj = -HFW; jj <= HFW;jj++){
///////////  first image:
					sumX  += (*(im1+(Y+ii)*H + (X+jj))) * d_mask[ (ii+HFW)%FW + (jj+HFW)*FW];//d_mask[1+ii]*a_mask[1+jj]; //*a_mask[1-1];
					sumY  += (*(im1+(Y+ii)*H + (X+jj))) * d_mask[ (jj+HFW)%FW + (ii+HFW)*FW];//a_mask[1+ii]*d_mask[1+jj]; //*a_mask[1-1];
					sumT1 += (*(im1+(Y+ii)*H + (X+jj))) * at_mask[(ii+HFW)%FW + (jj+HFW)*FW];//a_mask[1+ii]*a_mask[1+jj];   //*d_mask[1-1];
///////////  second image:					
  					sumX  += (*(im2+(Y+ii)*H + (X+jj))) *  d_mask[(ii+HFW)%FW + (jj+HFW)*FW];//d_mask[1+ii]*a_mask[1+jj];//*a_mask[1-1];
  					sumY  += (*(im2+(Y+ii)*H + (X+jj))) *  d_mask[(jj+HFW)%FW + (ii+HFW)*FW];//a_mask[1+ii]*d_mask[1+jj];//*a_mask[1-1];
					sumT2 += (*(im2+(Y+ii)*H + (X+jj))) * at_mask[(ii+HFW)%FW + (jj+HFW)*FW];//a_mask[1+ii]*a_mask[1+jj]; //d_mask[1+1];	 //notice the subtraction!	
				}
			}
            *(dx+(Y-HFW)*(W-HFW*2)+(X-HFW)) = (int32)(sumX);
            *(dy+(Y-HFW)*(W-HFW*2)+(X-HFW)) = (int32)(sumY);
            *(dt+(Y-HFW)*(W-HFW*2)+(X-HFW)) = (int32)(sumT1 -sumT2);
            if((X%2) && (Y%2)){
                 *(im1C+(Y/2)*WC+X/2) = (int32)(sumT1);
//                  *(im2C+(Y/2)*WC+X/2) = (int32)(sumT2);
            }
        }
    }
	
    // Coarse Convolution starts here
    for(Y=HFW; Y<HC-HFW; Y++)  {
        for(X=HFW; X<WC-HFW; X++)  {
			sumX = 0;  sumY = 0;  sumT1 = 0;sumT2 = 0;
			for (ii = -HFW; ii <= HFW;ii++){
				for (jj = -HFW; jj <= HFW;jj++){
///////////  first image:
					sumX  += (*(im1C+(Y+ii)*HC + (X+jj))) * d_mask[ (ii+HFW)%FW + (jj+HFW)*FW];//d_mask[1+ii]*a_mask[1+jj]; //*a_mask[1-1];
					sumY  += (*(im1C+(Y+ii)*HC + (X+jj))) * d_mask[ (jj+HFW)%FW + (ii+HFW)*FW];//a_mask[1+ii]*d_mask[1+jj]; //*a_mask[1-1];
					sumT1 += (*(im1C+(Y+ii)*HC + (X+jj))) * at_mask[(ii+HFW)%FW + (jj+HFW)*FW];//a_mask[1+ii]*a_mask[1+jj];   //*d_mask[1-1];
///////////  second image:					
  					sumX  += (*(im2C+(Y+ii)*HC + (X+jj))) *  d_mask[(ii+HFW)%FW + (jj+HFW)*FW];//d_mask[1+ii]*a_mask[1+jj];//*a_mask[1-1];
  					sumY  += (*(im2C+(Y+ii)*HC + (X+jj))) *  d_mask[(jj+HFW)%FW + (ii+HFW)*FW];//a_mask[1+ii]*d_mask[1+jj];//*a_mask[1-1];
					sumT1 -= (*(im2C+(Y+ii)*HC + (X+jj))) * at_mask[(ii+HFW)%FW + (jj+HFW)*FW];//a_mask[1+ii]*a_mask[1+jj]; //d_mask[1+1];	 //notice the subtraction!	
				}
			}
            *(dxC+(Y-HFW)*(WC-HFW*2)+(X-HFW)) = (int32)(sumX);
            *(dyC+(Y-HFW)*(WC-HFW*2)+(X-HFW)) = (int32)(sumY);
            *(dtC+(Y-HFW)*(WC-HFW*2)+(X-HFW)) = (int32)(sumT1);
//             if((X%2) && (Y%2)){
//                  *(im1C+(Y/2)*WC+X/2) = (int32)(sumT1);
//                  *(im2C+(Y/2)*WC+X/2) = (int32)(sumT2);
//             }
        }
    }
	
	
	return SUCCESS;
}

/*
 *  getData:  Gets data from a Matlab argument.
 *  Inputs: 
 *      const mxArray **prhs: Right hand side argument with RGB image
 *		(image *) Pointer to the black and white image struct.
 *
 *  Returns:
 *      int: 0 is successful run. -1 is a bad malloc. -2 is improper dims. 
 */
int getData(   const mxArray **prhs, progData *pD, int nrhs)
{ 
    static image bufIm;
    static image32 bufImC;
    static int W, H, WC, HC;
    //Interchange im1 and im2
    bufIm      = pD->bw_im2;
    pD->bw_im2 = pD->bw_im1;
    pD->bw_im1 = bufIm;
    bufImC      = pD->bw_im2C;
    pD->bw_im2C = pD->bw_im1C;
    pD->bw_im1C = bufImC;

    W = (int)mxGetN(prhs[0]);
    H = (int)mxGetM(prhs[0]);
    
    pD->bw_im1.dims[0] = W;
    pD->bw_im1.dims[1] = H;
    pD->bw_im2.dims[0] = W;
    pD->bw_im2.dims[1] = H;    
    
    pD->dx_im.dims[0] = W-HFW*2;
    pD->dx_im.dims[1] = H-HFW*2;
    pD->dy_im.dims[0] = W-HFW*2;
    pD->dy_im.dims[1] = H-HFW*2;
    pD->dt_im.dims[0] = W-HFW*2;
    pD->dt_im.dims[1] = H-HFW*2;    

    WC = pD->bw_im1.dims[0]/2-1;
    HC = pD->bw_im1.dims[1]/2-1;
    
    pD->bw_im1C.dims[0] = WC;
    pD->bw_im1C.dims[1] = HC;
    pD->bw_im2C.dims[0] = WC;
    pD->bw_im2C.dims[1] = HC;
    
    pD->dx_imC.dims[0] = WC-HFW*2;
    pD->dx_imC.dims[1] = HC-HFW*2;
    pD->dy_imC.dims[0] = WC-HFW*2;
    pD->dy_imC.dims[1] = HC-HFW*2;
    pD->dt_imC.dims[0] = WC-HFW*2;
    pD->dt_imC.dims[1] = HC-HFW*2;    
    
	memcpy(pD->bw_im1.im, (uint8 *)mxGetData(prhs[0]), sizeof(uint8)*pD->bw_im1.dims[0]*pD->bw_im1.dims[1]);
    if (nrhs > 1  ){
        memcpy(pD->bw_im2.im, (uint8 *)mxGetData(prhs[1]), sizeof(uint8)*pD->bw_im2.dims[0]*pD->bw_im2.dims[1]);
    }
    
    
    return SUCCESS;
}

/*
 *  sendData:  Sends data back to a Matlab argument.
 *  Inputs: 
 *      mxArray **plhs: Left hand side argument to get edge detected image
 *      (image *) Image to go back to Matlab.
 */
int sendData( mxArray *plhs[], progData *pD)
{

   int bytes_to_copy;
   int32 *start_of_pr;

    plhs[0] = mxCreateNumericArray(NDIMS,pD->dx_im.dims ,mxINT32_CLASS,mxREAL); 
	plhs[1] = mxCreateNumericArray(NDIMS,pD->dy_im.dims ,mxINT32_CLASS,mxREAL); 
	plhs[2] = mxCreateNumericArray(NDIMS,pD->dt_im.dims ,mxINT32_CLASS,mxREAL); 
    plhs[3] = mxCreateNumericArray(NDIMS,pD->dx_imC.dims,mxINT32_CLASS,mxREAL); 
	plhs[4] = mxCreateNumericArray(NDIMS,pD->dy_imC.dims,mxINT32_CLASS,mxREAL); 
	plhs[5] = mxCreateNumericArray(NDIMS,pD->dt_imC.dims,mxINT32_CLASS,mxREAL); 
                                  
    // Populate the the created array.
    start_of_pr = (int32 *) mxGetData(plhs[0]);
    bytes_to_copy = (int)(pD->dx_im.dims[0]*pD->dx_im.dims[1]* mxGetElementSize(plhs[0]));
    memcpy(start_of_pr, pD->dx_im.im, bytes_to_copy);
	
    start_of_pr = (int32 *) mxGetData(plhs[1]);
    bytes_to_copy = (int)(pD->dy_im.dims[0]*pD->dy_im.dims[1]* mxGetElementSize(plhs[1]));
    memcpy(start_of_pr, pD->dy_im.im, bytes_to_copy);
	
    start_of_pr = (int32 *) mxGetData(plhs[2]);
    bytes_to_copy = (int)(pD->dt_im.dims[0]*pD->dt_im.dims[1]* mxGetElementSize(plhs[2]));
    memcpy(start_of_pr, pD->dt_im.im, bytes_to_copy);

    
    start_of_pr = (int32 *) mxGetData(plhs[3]);
    bytes_to_copy = (int)(pD->dx_imC.dims[0]*pD->dx_imC.dims[1]* mxGetElementSize(plhs[3]));
    memcpy(start_of_pr, pD->dx_imC.im, bytes_to_copy);
	
    start_of_pr = (int32 *) mxGetData(plhs[4]);
    bytes_to_copy = (int)(pD->dy_imC.dims[0]*pD->dy_imC.dims[1]* mxGetElementSize(plhs[4]));
    memcpy(start_of_pr, pD->dy_imC.im, bytes_to_copy);
	
    start_of_pr = (int32 *) mxGetData(plhs[5]);
    bytes_to_copy = (int)(pD->dt_imC.dims[0]*pD->dt_imC.dims[1]* mxGetElementSize(plhs[5]));
    memcpy(start_of_pr, pD->dt_imC.im, bytes_to_copy);
    
    return SUCCESS;
} 
int allocateMem(progData *pD)
{
    
	if (((pD->bw_im1.im  = malloc(sizeof(uint8)*INWIDTH  *INWIDTH  )) == NULL )||
	   ( (pD->bw_im2.im  = malloc(sizeof(uint8)*INWIDTH  *INWIDTH  )) == NULL )||
	   ( (pD->dx_im.im   = malloc(sizeof(int32)*OUTWIDTH *OUTWIDTH )) == NULL )||
	   ( (pD->dy_im.im   = malloc(sizeof(int32)*OUTWIDTH *OUTWIDTH )) == NULL )||
	   ( (pD->dt_im.im   = malloc(sizeof(int32)*OUTWIDTH *OUTWIDTH )) == NULL )||
	   ( (pD->bw_im1C.im = malloc(sizeof(int32)*INWIDTHC *INWIDTHC )) == NULL )||
	   ( (pD->bw_im2C.im = malloc(sizeof(int32)*INWIDTHC *INWIDTHC )) == NULL )||
	   ( (pD->dx_imC.im  = malloc(sizeof(int32)*OUTWIDTHC*OUTWIDTHC)) == NULL )||
	   ( (pD->dy_imC.im  = malloc(sizeof(int32)*OUTWIDTHC*OUTWIDTHC)) == NULL )||
	   ( (pD->dt_imC.im  = malloc(sizeof(int32)*OUTWIDTHC*OUTWIDTHC)) == NULL ))
            return MALLOCFAIL;
	return SUCCESS;
}	
int deAllocateMem(progData *pD)
{
    free(pD->bw_im1.im);
    free(pD->bw_im2.im);
    free(pD->dx_im.im);
    free(pD->dy_im.im);		
    free(pD->dt_im.im);
    free(pD->bw_im1C.im);
    free(pD->bw_im2C.im);
    free(pD->dx_imC.im);
    free(pD->dy_imC.im);		
    free(pD->dt_imC.im);
    return 1;
}

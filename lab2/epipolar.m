clear
close all
% Implementation of the 8 point algorithm. The
% epipolar constraint is verified by drawing the
% epipolar lines associated to two points on the
% images (for further details Big�n 13.6)
%
% (C) F. Smeraldi, Oct 11th 2000
% Modified by M. Persson, Feb 8th 2006
%Modified by J. Bigun, Nov. 2011
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% These comments mark parts to be completed
%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Image size
XSIZE=768;
YSIZE=576;

%%%%%%%%%%%%%%%%%%%%
% Write the number of clicked points here (see below)
%%%%%%%%%%%%%%%%%%%%

NPOINTS= 15;

% first time you run this file, execute this line. Call "getCorresponds"
% after you called it once, then save the values of [pr, pl] by pasting them 
% into this file (in the same format as returned by the function below)
% [pr, pl] = getCorresponds(NPOINTS)

% % paste in from above, and remove "getCorresponds" from this file
pr =[
 231.7727  276.9959    1.0000
 288.8967  315.0785    1.0000
 296.0372  254.7810    1.0000
 350.7810  304.7645    1.0000
 476.1364  222.2521    1.0000
 471.3760  354.7479    1.0000
 567.3760  208.7645    1.0000
 577.6901  266.6818    1.0000
 534.8471  296.0372    1.0000
 736.3678   92.9298    1.0000
 427.7397   66.7479    1.0000
 411.8719   65.9545    1.0000
 126.2521   89.7562    1.0000
 134.1860  532.4669    1.0000
 215.1116  422.9793    1.0000
 ];
pl =[
 209.5579  246.8471    1.0000
 275.4091  276.9959    1.0000
 252.4008  223.8388    1.0000
 315.0785  264.3017    1.0000
 461.8554  182.5826    1.0000
 457.8884  302.3843    1.0000
 531.6736  168.3017    1.0000
 544.3678  221.4587    1.0000
 513.4256  249.2273    1.0000
 586.4174   68.3347    1.0000
 272.2355   52.4669    1.0000
 257.1612   51.6736    1.0000
 54.8471   75.4752    1.0000
 64.3678  499.9380    1.0000
 245.2603  380.1364    1.0000
 ];
% pr = [ 288.0208  314.0625    1.0000
%   227.6042  278.6458    1.0000
%   469.2708  221.3542    1.0000
%   655.7292  423.4375    1.0000
%   214.0625  419.2708    1.0000
%   132.8125  526.5625    1.0000
%   126.5625   92.1875    1.0000
%   736.9792   92.1875    1.0000
%   425.5208   67.1875    1.0000
%   407.8125   66.1458    1.0000
%   753.6458   45.3125    1.0000
%   116.1458   41.1458    1.0000
%   534.8958  302.6042    1.0000
%   397.3958  297.3958    1.0000
%   293.2292  253.6458    1.0000
%   ];
% pl = [
%     
%   273.4375  276.5625    1.0000
%   208.8542  249.4792    1.0000
%   456.7708  184.8958    1.0000
%   606.7708  349.4792    1.0000
%   244.2708  379.6875    1.0000
%    64.0625  501.5625    1.0000
%    55.7292   76.5625    1.0000
%   583.8542   69.2708    1.0000
%   270.3125   53.6458    1.0000
%   256.7708   52.6042    1.0000
%   600.5208   25.5208    1.0000
%    44.2708   30.7292    1.0000
%   513.0208  249.4792    1.0000
%   391.1458  253.6458    1.0000
%   252.6042  225.5208    1.0000
% ];
% pr =[ 215.1042  424.4792    1.0000
%   657.8125  419.2708    1.0000
%   288.0208  313.0208    1.0000
%   132.8125  531.7708    1.0000
%   125.5208   91.1458    1.0000
%   738.0208   93.2292    1.0000
%   478.6458  279.6875    1.0000
%   386.9792  370.3125    1.0000
%   409.8958   66.1458    1.0000
%   110.9375   41.1458    1.0000
%   425.5208  207.8125    1.0000
%   455.7292  588.0208    1.0000
%   227.6042  278.6458    1.0000
%   292.1875  252.6042    1.0000
%   283.8542  255.7292    1.0000 ];
% pl =[ 243.2292  377.6042    1.0000
%   606.7708  346.3542    1.0000
%   272.3958  278.6458    1.0000
%    64.0625  497.3958    1.0000
%    54.6875   78.6458    1.0000
%   583.8542   68.2292    1.0000
%   471.3542  238.0208    1.0000
%   343.2292  326.5625    1.0000
%   255.7292   50.5208    1.0000
%    41.1458   31.7708    1.0000
%   409.8958  172.3958    1.0000
%   450.5208  525.5208    1.0000
%   204.6875  248.4375    1.0000
%   253.6458  222.3958    1.0000
%   245.3125  228.6458    1.0000
%  ];


figure(1);set(gcf,'Name','LEFT IMAGE'); hold on;
imshow('.\images\left.bmp');title('Left');
for k=1:NPOINTS
text(pl(k,1),pl(k,2),['* ' num2str(k)],'FontSize',10,'color','r'); %OPTIONAL visualize what you clicked
end

figure(2);set(gcf,'Name','RIGHT IMAGE');hold on
imshow('.\images\right.bmp');title('Right');
for k=1:NPOINTS
text(pr(k,1),pr(k,2),['* ' num2str(k)],'FontSize',10,'color','g'); %OPTIONAL visualize what you clicked
end


% %FIRST DO THE EXERCISE WITHOUT NORMALIZATION, I.E. KEEP THE COMMENTS LIKE
%                       "Normaliztion consequence   x" 
% %in place....
% %Normalization consequence 1
% % Construct matrices hl and hr to normalize these coordinates
% % so that the average of each component is 0 
% %(forcing the centroid to be the origin) and its variance
% % is normalized to 1 (making the lengths be represented more accurately)
plr=[pl; pr];
plr_msd=plr(:,1)+1i*plr(:,2);
d= mean(abs(plr_msd-mean(plr_msd)));
d_l = std(pl);
d_r = std(pr);
% %%%%%%%%%%%%%%%%%%%%%
% % First construct hl...
% %%%%%%%%%%%%%%%%%%%%%
hl=[
    1/d   0  -mean(pl(:,1))/d
    0    1/d  -mean(pl(:,2))/d
    0     0    1
    ];
% hl=[
%     1/d_l(1)   0  -mean(pl(:,1))/d_l(1)
%     0    1/d_l(2)  -mean(pl(:,2))/d_l(2)
%     0     0    1
%     ];
% %%%%%%%%%%%%%%%%%%%%%
% % ...and then hr:
% %%%%%%%%%%%%%%%%%%%%%
hr=[
    1/d   0  -mean(pr(:,1))/d
    0    1/d  -mean(pr(:,2))/d
    0     0    1
    ];
% hr=[
%     1/d_r(1)   0  -mean(pr(:,1))/d_r(1)
%     0    1/d_r(2)  -mean(pr(:,2))/d_r(2)
%     0     0    1
%     ];
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Compute the normalized coordinates
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
prhat=pr;  
plhat=pl;

pl=(hl*pl')';
pr=(hr*pr')';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now compute Ftilde from Q.
% Row i of the matrix Q of the coefficients is obtained
% from the components in prhat(i,:) and plhat(i,:).
% So first create matrix Q (equations 13.125 to 13.129 should give you an
% idea) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...
for i=1:NPOINTS
    Q(i,:) = [pr(i,1)*pl(i,:) pr(i,2)*pl(i,:) pr(i,3)*pl(i,:)];
end


%%%%%%%%%%%%%%%%%%%%%
% And then compute the total least square solution of Q*Ftilde=0, which is given by the
%eigenvector corresponding to the smallest eigenvalue
% of Q'*Q or alternatively you can use the SVD decomposition (Q=U*S*V')for this.
%%%%%%%%%%%%%%%%%%%%%
%...
[U, S, V] = svd(Q);
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape vector  Ftilde into  matrix F. 
%%%%%%%%%%%%%%%%%%%%%%%%%%
%...
f = V(:,9);
F = reshape(f, 3, 3)';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure that F is singular: first compute its SVD, i.e.  
%find UF, SF, VF such that   UF*SF*VF'=F
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...

[UF, SF, VF] = svd(F);

%%%%%%%%%%%%%%%%%%%%%%%
% Then enforce singularity by setting the smallest singular value to
% zero
%%%%%%%%%%%%%%%%%%%%%%%
%...

SF(3,3) = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ... and finally recompute F
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...
F = UF*SF*VF';


% %Normalization consequence 3 
% %Now we get our real F that takes in unnormalized coordinates from left and
% %right to produce a scalar...a sandwich.
F=hr'*F*hl;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The left epipole lies in the null space of F; the right epipole
% belongs to the null space of F'. You can obtain them directly
% from the SVD of F you have just computed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...

 disp('Left epipole');
 el= VF(3,:);
 disp('Right epipole');
 er= UF(3,:);
%### Verification of the epipolar constraint using pr'*F*pl=0 ###
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verify the epipolar constraint: choose a point,  in one image
% (say the right first). Draw the corresponding epipolar line on the other
% image where the epipolar line equation is
% fullfilled. Verify that the line passes through the corresponding point in 
% the other image.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x=1:XSIZE; %Let x coordinates run all possible column coordinates
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% Below are the epipolar lines on the left image
		% associated to points pr(%%,:) on the right image.
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Obtain the coefficients of the the first line equation in the vector, abc
figure(1);hold on;
for i=1:15
    abc=prhat(i,:)*F;    %This is obtained by inspecting pr'*F*pl=0, for the point pr(1,:).
    abc=abc/abc(2); %We make sure that the coefficient of y in the line-equation is 1
    %We assumed that abc(2) is not zero
    y=-abc(1)*x - abc(3);
    plot(x,y,'g');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% Do the same with another point on the right image
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %...	
	    
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% Now draw on the right image the epipolar lines
		% associated with  two points
		% on the left image
		%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Write the two images with the epipolar lines to the disk
% figure(1);Im=getframe; imwrite(Im.cdata,'left.jpg')
% figure(2);Im=getframe; imwrite(Im.cdata,'right.jpg')
diagnoal = diag(pr*F*pl');
len = norm(diagnoal)
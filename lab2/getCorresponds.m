function [pr, pl] = getCorresponds(NPOINTS)

uiwait(helpdlg(...
    'This dialog pops up when calling the function "getCorresponds". You will now click points for correspondances between 2 images of differing viewpoints. After you close down ths dialog, click points of interest in the left image, then click the same point in the right. Do this for N points, then save the resulting matrices by pasting them into "epipolar.m"'...
    ));


if nargin< 1
    NPOINTS= 15;
end

% Image size
XSIZE=768;
YSIZE=576;

figure(1);set(1,'Name','LEFT IMAGE'); hold on;
imshow('.\images\left.bmp');title('Left');

figure(2);set(2,'Name','RIGHT IMAGE');hold on
imshow('.\images\right.bmp');title('Right');

 pl=[]; pr=[];
for k=1:NPOINTS
% Left image
figure(1); %put focus on the left image for clicking...
onepl= ginput(1);
    pl= [pl
        onepl 1 
     ];
text(pl(k,1),pl(k,2),['* ' num2str(k)],'FontSize',10,'color','r'); %OPTIONAL visualize what you clicked
 
 
% Right image
figure(2); %put focus on the right image  for clicking...
onepr= ginput(1);
    pr= [pr
        onepr 1 %%%
     ];
text(pr(k,1),pr(k,2),['* ' num2str(k)],'FontSize',10,'color','g'); %OPTIONAL visualize what you clicked
     
end

close all;
